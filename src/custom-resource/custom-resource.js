import _ from 'lodash';
import BaseComponent from 'formiojs/components/base/Base';
import Components from 'formiojs/components/Components';
import CustomResourceForm from './custom-resource.form';

export default class CustomResourceComponent extends BaseComponent {
  static schema(...extend) {
    return BaseComponent.schema(
      {
        type: 'test',
        label: 'Test',
        key: 'test',
      },
      ...extend,
    );
  }

  static get builderInfo() {
    return {
      title: 'TEEEEEST',
      icon: 'fa fa-list-alt',
      group: 'basic',
      documentation: 'http://help.form.io/userguide/#panels',
      weight: 0,
      schema: CustomResourceComponent.schema(),
    };
  }

  constructor(component, options, data) {
    super(component, options, data);
    console.log('_ :', _.add(1, 1));
    console.log('ready :');
  }
}

// Use the table component edit form.
CustomResourceComponent.editForm = CustomResourceForm;

// Register the component to the Formio.Components registry.
Components.addComponent('test', CustomResourceComponent);
