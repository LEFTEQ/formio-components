import baseEditForm from 'formiojs/components/base/Base.form';

import CustomResourceEditDisplay from './editForm/custom-resource.edit.display';

export default function(...extend) {
  return baseEditForm(
    [
      {
        key: 'display',
        components: CustomResourceEditDisplay,
      },
    ],
    ...extend,
  );
}
