import { terser } from 'rollup-plugin-terser';

export default [
  {
    input: 'src/main.js',
    plugins: [terser()],
    output: {
      file: 'umd/ntx-formio-components.js',
      format: 'umd',
      name: 'formComponents',
      esModule: false,
    },
  },
  {
    input: {
      index: 'src/main.js',
      customResource: 'src/custom-resource/custom-resource.js',
    },
    output: [
      {
        dir: 'esm',
        format: 'esm',
      },
      {
        dir: 'cjs',
        format: 'cjs',
      },
    ],
  },
];
